/* Copyright 2021 Dominik George <dominik.george@teckids.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

use crate::config::{
    get_config,
    get_optional,
};
use config::Config;
use crate::cache::get_context_user;

use crate::logging::setup_log;

use crate::oauth::{get_data_jq, get_usable_token};

use libnss::interop::Response;
use libnss::passwd::{PasswdHooks, Passwd};
use crate::unix::PasswdHelper;

fn nss_hook_prepare() -> Config {
    let conf = get_config(None);

    let mut log_level = log::LevelFilter::Error;
    if get_optional(&conf, "nss.debug").unwrap_or_default() {
        log_level = log::LevelFilter::Debug;
    }
    setup_log(log_level);

    // Set the context user to the current user, but only if not already set
    // When doing PAM, we might get called back into by libc to do some NSS
    // lookup, and we want to keep the PAM login user context in that case
    if !get_context_user().is_initialized() {
        get_context_user().set_current_user();
    }

    return conf;
}

struct OidcPasswd;

impl PasswdHooks for OidcPasswd {
    fn get_all_entries() -> Response<Vec<Passwd>> {
        let conf = nss_hook_prepare();
        info!("[NSS] passwd.get_all_entries called");

        let token = match get_usable_token(&conf, "nss", &mut get_context_user()) {
            Some(t) => t,
            None => {
                error!("Failed to get usable access token");
                return Response::Unavail;
            }
        };

        let data: Vec<PasswdHelper> = match get_data_jq(&conf, "nss", "passwd.list", "".to_string(), &token, true) {
            Ok(d) => d,
            Err(e) => {
                error!("Could not load JSON data for passwd: {}", e);
                return Response::Unavail;
            }
        };
        Response::Success(data.into_iter().map(|p| p.0).collect())
    }

    fn get_entry_by_uid(uid: libc::uid_t) -> Response<Passwd> {
        let conf = nss_hook_prepare();
        info!("[NSS] passwd.get_entry_by_uid called for {}", uid);

        let token = match get_usable_token(&conf, "nss", &mut get_context_user()) {
            Some(t) => t,
            None => {
                error!("Failed to get usable access token");
                return Response::Unavail;
            }
        };

        let data: Passwd = match get_data_jq(&conf, "nss", "passwd.by_uid", uid, &token, false).map(|PasswdHelper(p)| p) {
            Ok(d) => d,
            Err(e) => {
                error!("Could not load JSON data for passwd: {}", e);
                return Response::NotFound;
            }
        };
        Response::Success(data)
    }

    fn get_entry_by_name(name: String) -> Response<Passwd> {
        let conf = nss_hook_prepare();
        info!("[NSS] passwd.get_entry_by_name called for {}", name);

        let token = match get_usable_token(&conf, "nss", &mut get_context_user()) {
            Some(t) => t,
            None => {
                error!("Failed to get usable access token");
                return Response::Unavail;
            }
        };

        let data: Passwd = match get_data_jq(&conf, "nss", "passwd.by_name", name, &token, false).map(|PasswdHelper(p)| p) {
            Ok(d) => d,
            Err(e) => {
                error!("Could not load JSON data for passwd: {}", e);
                return Response::NotFound;
            }
        };
        Response::Success(data)
    }
}

libnss_passwd_hooks!(webapi, OidcPasswd);
